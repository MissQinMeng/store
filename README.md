# Store系统介绍





## 技术栈

技术 | 说明 | 官网
----|----|----
lombok | 一个 Java 库，可自动插入您的编辑器并构建工具  | [https://projectlombok.org/](https://projectlombok.org/)
mysql | 一个免费最常用的关系型数据库 | [https://www.mysql.com/](https://www.mysql.com/)
redis | 最强大的内存管理数据库 | [https://redis.io/](https://redis.io/)
druid | 一个JDBC连接池,监控,等组件 | [https://github.com/alibaba/druid](https://github.com/alibaba/druid)
hutool | Hutool是一个小而全的Java工具类库 | [https://www.hutool.cn/docs/#/](https://www.hutool.cn/docs/#/)
mybatis plus | 非常好用的dao层框架 增强mybatis | [https://mybatis.plus/](https://mybatis.plus/)
autopoi | Excel和 Word简易工具类 | [https://gitee.com/jeecg/autopoi?_from=gitee_search](https://gitee.com/jeecg/autopoi?_from=gitee_search)



# 模块说明

1. core 基础依赖模块(配置,工具类等)
2. auth 权限模块
3. store 店铺模块
4. system 系统级别处理模块(垃圾表处理)

# 常用类说明

|  类   | 说明  |
|  ----  | ----  |
| DeleteInnerInterceptor  | 删除拦截器 |
| WebLogAop  | aop日志处理 |
| RequestInterceptor  | 请求拦截 |
| BasePageRequest  | 分页统一请求父类 |
| BaseRequest  | 统一请求父类 |
| BaseResult  | 统一返回类 |
| RedisKeyConst  | RedisKey管理类 |
| RequestAttributeKeyConst  | RequestAttributeKey管理类 |

## entity包
1. base 基础类
2. bo 数据库对应实体
3. dto 参数和传输对应
4. vo  返回实体


# 运行环境
- jdk11
- mysql 8
- redis 5.0



# 规范
com.time.store.\*.controller 控制器,负责对service的前置处理, 处理对象转换之类

com.time.store.\*.service service无接口类 直接实现类,具体业务逻辑

com.time.store.\*.dao 对数据库的操作

com.time.store.\*.entity.bo 数据库对应实体

com.time.store.\*.entity.dto 接受的请求或者传输需要的类

com.time.store.\*.entity.vo 响应类,返回类



## 控制器返回
### 错误返回直接抛异常
示例
```java
        import com.time.store.core.util.Assert;
        Assert.state(false,"失败");
        //返回{"code":500,"success":false,"message":"java.lang.IllegalStateException: 失败","data":null}
```
### 正常返回
```java
    // 直接传字符串会封装到message里
    @GetMapping
    public String hello() {
        return "hello";
    }
    //返回{"code":200,"message":"hello","success":true}

    // 传除了字符串和Result其他类会封装到data里
    @GetMapping
    public JSONObject hello() {
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("name","jikeytang");
        return jsonObject;
    }
    // 返回{"code":200,"data":{"name":"jikeytang"},"success":true}
```
### 自定义返回
```java
    // 自定义result返回
    @GetMapping
    public Result hello() {
        return new Result();
    }
    
    //如果不想返回Result又想自定义可以使用@IgnoreResultAdvice
    @GetMapping
    @IgnoreResultAdvice
    public Result hello() {
        return new Result();
    }
```

## 注解
注解统一放在com.yun.yunmanager.annotations下
1. @IgnoreAuth 忽略权限校验可放在方法或者类上
2. @IgnoreResultAdvice 忽略对返回值封装可放在方法或者类上
3. @LoginUser 控制器方法属性使用，可以注入当前用户
```java
  public ActiveUser me(@LoginUser ActiveUser activeUser) {
        return activeUser;
    }
```

## sql规范

1. 设计表所有字段尽量不能为null
2. 表名规范  模块名_表名
3. 创建时间和修改时间必带
```sql
    `create_time` datetime NOT NULL COMMENT '创建时间',
    `update_time` datetime NOT NULL COMMENT '修改时间',
```
对应model
```java
    @TableId(type = IdType.AUTO)
    private Integer id;
```
4. id为int类型
```sql
    `id` int NOT NULL,
```
对应model
```java
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
```
5. 不需要是否删除状态,被删除的记录会统一存在删除表里


