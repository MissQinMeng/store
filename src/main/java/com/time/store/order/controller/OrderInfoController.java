package com.time.store.order.controller;

import com.alibaba.fastjson.serializer.CharacterCodec;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.time.store.order.entity.bo.OrderInfo;
import com.time.store.order.entity.dto.OrderPageExportReq;
import com.time.store.order.entity.dto.OrderPageQuery;
import com.time.store.order.entity.dto.OrderStarDTO;
import com.time.store.order.entity.vo.OrderProductResp;
import com.time.store.order.service.OrderInfoService;
import org.apache.poi.ss.usermodel.Workbook;
import org.jeecgframework.poi.excel.ExcelExportUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;

/**
 * 订单信息控制器
 *
 * @description: 订单信息
 * @menu 订单
 */
@RestController
@RequestMapping("/api/order")
public class OrderInfoController {

    @Autowired
    private OrderInfoService orderInfoService;

    /**
     * 分页查询订单
     *
     * @param orderPageQuery
     * @return
     * @status done
     */
    @GetMapping("page")
    public Page<OrderProductResp> page(@Validated OrderPageQuery orderPageQuery) {
        return orderInfoService.pageOrder(orderPageQuery);
    }

    /**
     * 导出订单
     *
     * @param orderPageExportReq
     * @return
     * @status done
     */
    @GetMapping("export")
    public void exportOrder(OrderPageExportReq orderPageExportReq, HttpServletResponse response) throws IOException {
        Workbook workbook = orderInfoService.exportOrder(orderPageExportReq,response);
        workbook.write(response.getOutputStream());
    }

    /**
     * 加星订单
     *
     * @param orderStarDTO
     * @return
     * @status done
     */
    @PatchMapping("/star")
    public void star(@RequestBody @Validated OrderStarDTO orderStarDTO){
        orderInfoService.star(orderStarDTO);
    }
}
