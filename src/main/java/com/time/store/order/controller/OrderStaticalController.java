package com.time.store.order.controller;

import com.time.store.order.entity.vo.HomeOverviewVO;
import com.time.store.order.entity.vo.OrderOverviewVO;
import com.time.store.order.service.OrderDayStaticalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 订单统计控制器
 *
 * @description: 订单统计
 * @menu 订单统计
 */
@RestController
@RequestMapping("/api/order/statical")
public class OrderStaticalController {
    @Autowired
    private OrderDayStaticalService orderDayStaticalService;

    /**
     * 获取店铺订单统计信息
     *
     * @return
     * @status done
     */
    @GetMapping("/yesterday")
    public HomeOverviewVO yesterday() {
        return orderDayStaticalService.getYesterday();
    }

    /**
     * 订单概况统计
     * @status done
     * @return
     */
    @GetMapping("/orderOverview")
    public OrderOverviewVO orderOverview() {
        return orderDayStaticalService.orderOverviewVO();
    }



}
