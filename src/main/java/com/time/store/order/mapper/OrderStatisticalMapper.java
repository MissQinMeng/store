package com.time.store.order.mapper;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.time.store.order.entity.bo.OrderDayStatistical;
import com.time.store.order.entity.bo.OrderStatistical;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDate;

/**
 * 订单统计mapper
 */
public interface OrderStatisticalMapper extends BaseMapper<OrderStatistical> {
    /**
     * 根据店铺获取订单统计
     * @param
     * @return
     */
    OrderStatistical getByStoreId();




}
