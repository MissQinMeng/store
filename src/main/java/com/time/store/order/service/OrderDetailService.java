package com.time.store.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.time.store.order.entity.bo.OrderDetail;
import com.time.store.order.entity.bo.OrderStatistical;
import com.time.store.order.mapper.OrderDetailMapper;
import com.time.store.order.mapper.OrderStatisticalMapper;
import org.springframework.stereotype.Service;

@Service
public class OrderDetailService extends ServiceImpl<OrderDetailMapper, OrderDetail> implements IService<OrderDetail> {
}
