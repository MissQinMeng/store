package com.time.store.order.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.time.store.core.util.AssertUtil;
import com.time.store.order.entity.bo.OrderAfterSale;
import com.time.store.order.entity.bo.OrderStatistical;
import com.time.store.order.entity.dto.AfterSalePageQuery;
import com.time.store.order.entity.dto.OrderAfterSaleExportDTO;
import com.time.store.order.entity.dto.OrderStandardExportDTO;
import com.time.store.order.entity.dto.ReimburseDTO;
import com.time.store.order.mapper.OrderAfterSaleMapper;
import com.time.store.order.mapper.OrderStatisticalMapper;
import org.apache.poi.ss.usermodel.Workbook;
import org.jeecgframework.poi.excel.ExcelExportUtil;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.List;

@Service
public class OrderAfterSaleService extends ServiceImpl<OrderAfterSaleMapper, OrderAfterSale> implements IService<OrderAfterSale> {

    /**
     * 分页查询
     *
     * @param afterSalePageQuery
     * @return
     */
    public Page<OrderAfterSale> pageAfterSale(AfterSalePageQuery afterSalePageQuery) {
        Page<OrderAfterSale> page = new Page<>(afterSalePageQuery.getCurrent(), afterSalePageQuery.getSize());
        return baseMapper.pageAfterSale(afterSalePageQuery, page);
    }

    /**
     * 导出
     *
     * @param afterSalePageQuery
     * @return
     */
    public Workbook export(AfterSalePageQuery afterSalePageQuery) {
        ExportParams params = new ExportParams("售后报表", "售后报表");
        List<OrderAfterSaleExportDTO> orderAfterSaleExportDTOS = baseMapper.afterSale(afterSalePageQuery);
        return ExcelExportUtil.exportExcel(params, OrderAfterSaleExportDTO.class, orderAfterSaleExportDTOS);

    }

    /**
     * 退款
     *
     * @param reimburseDTO
     */
    public void reimburse(ReimburseDTO reimburseDTO) {
        OrderAfterSale orderAfterSale = getById(reimburseDTO.getId());
        AssertUtil.notNull(orderAfterSale, "未查询到售后记录");
        if (reimburseDTO.getIsAgree()) {
            orderAfterSale.setStatus(4);
            orderAfterSale.setReceiverProvince(reimburseDTO.getReceiverProvince());
            orderAfterSale.setReceiverCity(reimburseDTO.getReceiverCity());
            orderAfterSale.setReceiverArea(reimburseDTO.getReceiverArea());
            orderAfterSale.setReceiverAddress(reimburseDTO.getReceiverAddress());
        } else
            orderAfterSale.setStatus(3);

        baseMapper.updateById(orderAfterSale);

    }
}
