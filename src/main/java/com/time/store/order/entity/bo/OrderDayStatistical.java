package com.time.store.order.entity.bo;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDate;
import java.util.Date;

/**
 * <p>
 * 订单没人统计表
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
@EqualsAndHashCode(callSuper = true)
@TableName("order_day_statistical")
@Data
public class OrderDayStatistical extends Model<OrderDayStatistical> {

    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;


    /**
     * 待发货订单数
     */
    private Integer forwardingOrderNum;

    /**
     * 维权订单数
     */
    private Integer rightsOrderProtectionNum;

    /**
     * 今日订单数
     */
    private Integer orderNum;
    /**
     * 今日支付订单数
     */
    private Integer paymentOrderNum;

    /**
     * 今日交易额
     */
    private Integer transactionAmount;

    /**
     * 今日收入
     */
    private Integer income;
    /**
     * 统计日期
     */
    private LocalDate statisticalDate;
    /**
     * 店铺id
     */
    private Integer storeId;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;


}
