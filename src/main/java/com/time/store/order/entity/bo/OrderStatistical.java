package com.time.store.order.entity.bo;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * <p>
 * 订单统计表
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
@EqualsAndHashCode(callSuper = true)
@TableName("order_statistical")
@Data
public class OrderStatistical extends Model<OrderStatistical> {

    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 待发货订单数
     */
    private Integer toBeDeliveredOrderNum;

    /**
     * 维权订单数
     */
    private Integer rightsProtectionOrderNum;

    /**
     * 待付款订单数
     */
    private Integer pendingPaymentOrderNum;
    /**
     * 店铺id
     */
    private Integer storeId;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;


}
