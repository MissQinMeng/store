package com.time.store.order.entity.dto;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * 订单导出请求
 */
@Data
public class OrderPageExportReq extends OrderPageQuery{
    /**
     * 导出报表类型(1:标准报表 2:订单维度 3:商品维度)
     */
    @NotNull
    @Min(1)
    @Max(3)
    private Integer exportType;

}
