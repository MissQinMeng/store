package com.time.store.order.entity.dto;

import lombok.Data;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelIgnore;
import org.jeecgframework.poi.excel.annotation.ExcelTarget;

@Data
@ExcelTarget("orderExportShipmentDto")
public class OrderExportShipmentDto {
    /**
     * 订单ID
     */

    @Excel(name = "订单ID")
    private Integer orderId;
    /**
     * 物流编号
     */
    @Excel(name = "物流编号")
    private String tackingNo;
    /**
     * 物流名称 中通,申通 等
     */
    @Excel(name = "物流名称")
    private String tackingName;
}
