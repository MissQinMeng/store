package com.time.store.order.entity.dto;


import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.time.store.core.consts.DateTimeFormatConst;
import lombok.Data;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelIgnore;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

@Data
public class OrderAfterSaleExportDTO {

    /**
     * id
     */
    @ExcelIgnore
    private Integer id;

    /**
     * 售后编号
     */
    @Excel(name = "售后编号")
    private String afterSaleNo;

    /**
     * 商品id
     */
    @ExcelIgnore
    private Integer productId;

    /**
     * 商品名称
     */
    @Excel(name = "商品名称")
    private Integer productName;

    /**
     * 退货用户id
     */
    @ExcelIgnore
    private Integer membersId;

    /**
     * 订单id
     */
    @ExcelIgnore
    private Integer orderId;

    /**
     * 订单code
     */
    @Excel(name = "订单code")
    private String orderNo;

    /**
     * 订单详情id
     */
    @ExcelIgnore
    private Integer orderDetailId;

    /**
     * 退款类型 (1:售中退款 2:售后退款)
     */
    @Excel(name = "退款类型",replace = {"售中退款_1","售后退款_2"})
    private int refundType;

    /**
     * 有赞客服介入状态 (1:未介入 2:介入中 3:介入结束)
     */
    @Excel(name = "有赞客服介入状态",replace = {"未介入_1","介入中_2","介入结束_3"})
    private int interveneStatus;

    /**
     * 售后方式 (1:仅退款 2:退货退款 3:换货)
     */
    @Excel(name = "售后方式",replace = {"仅退款_1","退货退款_2","换货_3"})
    private int manner;

    /**
     * 发货状态 (1:未发货 2:已发货)
     */
    @Excel(name = "发货状态",replace = {"未发货_1","已发货_2"})
    private int deliveryStatus;

    /**
     * 售后状态 (1:售后处理中 2:售后申请待商家同意 3:商家不同意售后申请，待买家处理 4:商家同意售后申请，待买家处理 5:买家已退货 6:待商家确认收货 7:商家拒绝收货，待买家处理 8:商家已发货，待买家确认收货，9:售后成功 10:售后关闭)
     */
    @Excel(name = "售后状态",replace = {"售后处理中_1","售后申请待商家同意_2","商家不同意售后申请，待买家处理_3","商家同意售后申请，待买家处理_4","买家已退货_5","待商家确认收货_6","商家拒绝收货，待买家处理_7","商家已发货，待买家确认收货_8","售后成功_9","售后关闭_10"})
    private int status;

    /**
     * 物流编号
     */
    @Excel(name = "物流编号")
    private String tackingNo;

    /**
     * 物流状态 (1:未签收 2:已签收 3:无物流信息)
     */
    @Excel(name = "物流状态",replace = {"未签收_1","已签收_2","无物流信息_3"})
    private int tackingStatus;

    /**
     * 订单金额
     */
    @Excel(name = "订单金额")
    private BigDecimal orderDetailAmount;

    /**
     * 退款金额
     */
    @Excel(name = "退款金额")
    private BigDecimal refundAmount;

    /**
     * 超时时间
     */
    @Excel(name = "超时时间",exportFormat= DateTimeFormatConst.DEFAULT_DATETIME_PATTERN)
    private Date overtimeTime;

    /**
     * 收货人姓名
     */
    @ExcelIgnore
    private String receiverName;

    /**
     * 收货人手机号
     */
    @ExcelIgnore
    private String receiverPhone;

    /**
     * 收货人手机号后四位
     */
    @ExcelIgnore
    private String receiverPhoneLastFour;

    /**
     * 收货人省
     */
    @ExcelIgnore
    private String receiverProvince;

    /**
     * 收货人市
     */
    @ExcelIgnore
    private String receiverCity;

    /**
     * 收货人区
     */
    @ExcelIgnore
    private String receiverArea;
    /**
     * 收货人详细地址
     */
    @ExcelIgnore
    private String receiverAddress;

    /**
     * 店铺id
     */
    @ExcelIgnore
    private Integer storeId;

    @Excel(name = "创建时间",exportFormat= DateTimeFormatConst.DEFAULT_DATETIME_PATTERN)
    private Date createTime;

    @ExcelIgnore
    private Date updateTime;


    public void setOrderDetailAmount(BigDecimal orderDetailAmount) {
        this.orderDetailAmount = orderDetailAmount.divide(BigDecimal.valueOf(100),2, RoundingMode.HALF_UP);;
    }

    public void setRefundAmount(BigDecimal refundAmount) {
        this.refundAmount =  refundAmount.divide(BigDecimal.valueOf(100),2, RoundingMode.HALF_UP);;
    }
}
