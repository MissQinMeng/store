package com.time.store.order.entity.bo;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * <p>
 * 订单统计表
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
@EqualsAndHashCode(callSuper = true)
@TableName("order_detail")
@Data
public class OrderDetail extends Model<OrderDetail> {

    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 订单id
     */
    private Integer orderId;

    /**
     * 订单编号
     */
    private String orderNo;

    /**
     * 商品id
     */
    private Integer productId;

    /**
     * 商品编号
     */
    private String productNo;

    /**
     * 商品名称
     */
    private String productName;

    /**
     * 商品数量
     */
    private Integer qty;

    /**
     * 应收金额 单位分
     */
    private Integer receivableAmount;

    /**
     * 优惠金额 单位分
     */
    private Integer favorableAmount;

    /**
     * 实收金额 单位分
     */
    private Integer paidAmount;

    /**
     * 商品sku信息
     */
    private String productSku;

    /**
     * 优惠券id
     */
    private Integer couponId;

    /**
     * 店铺id
     */
    private Integer storeId;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;


}
