package com.time.store.order.entity.dto;


import lombok.Data;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecgframework.poi.excel.annotation.ExcelIgnore;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

@Data
public class OrderProductStandardExportDTO {
    /**
     * 订单详情id
     */
    @ExcelIgnore
    private Integer orderDetailId;
    /**
     * 商品id
     */
    @ExcelIgnore
    private Integer productId;

    /**
     * 商品编号
     */
    @Excel(name = "商品编号")
    private String productNo;

    /**
     * 商品名称
     */
    @Excel(name = "商品名称")
    private String productName;

    /**
     * 商品数量
     */
    @Excel(name = "商品数量")
    private Integer qty;

    /**
     * 应收金额
     */
    @Excel(name = "应收金额")
    private BigDecimal receivableAmount;

    /**
     * 优惠金额
     */
    @Excel(name = "优惠金额")
    private BigDecimal favorableAmount;

    /**
     * 实收金额
     */
    @Excel(name = "实收金额")
    private BigDecimal paidAmount;

    /**
     * 商品sku信息
     */
    @Excel(name = "商品sku信息")
    private String productSku;


    public void setReceivableAmount(BigDecimal receivableAmount) {
        this.receivableAmount = receivableAmount.divide(BigDecimal.valueOf(100),2, RoundingMode.HALF_UP);
    }

    public void setFavorableAmount(BigDecimal favorableAmount) {
        this.favorableAmount = favorableAmount.divide(BigDecimal.valueOf(100),2, RoundingMode.HALF_UP);

    }

    public void setPaidAmount(BigDecimal paidAmount) {
        this.paidAmount = paidAmount.divide(BigDecimal.valueOf(100),2, RoundingMode.HALF_UP);
    }

}
