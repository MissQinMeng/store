package com.time.store.order.entity.dto;

import lombok.Data;
import org.jeecgframework.poi.excel.annotation.Excel;

@Data
public class OrderImportShipmentDto {


    /**
     * 订单编号
     */
    @Excel(name = "订单编号")
    private Integer orderNo;

    /**
     * 物流编号
     */
    @Excel(name = "物流编号")
    private String tackingNo;
    /**
     * 物流名称 中通,申通 等
     */
    @Excel(name = "物流名称")
    private String tackingName;


}
