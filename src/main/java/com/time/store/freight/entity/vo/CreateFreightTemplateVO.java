package com.time.store.freight.entity.vo;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

/**
 * 创建运费模板
 */
@Data
public class CreateFreightTemplateVO {
    /**
     * 运费模板id
     */
    private Integer id;
    /**
     * 运费模板名称
     */
    private String name;
    /**
     * 计费方式 (1:按件数 2:按重量)
     */
    private Integer billingMethod;

    /**
     * 创建时间
     */
    private Date createTime;
    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     * 配送区域
     */
    private List<CreateFreightTemplateAreaVO> templateArea;
}
