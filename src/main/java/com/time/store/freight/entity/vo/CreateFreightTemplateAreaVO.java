package com.time.store.freight.entity.vo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * 创建运费模板区域
 */
@Data
public class CreateFreightTemplateAreaVO {
    /**
     * 模板区域id
     */
    private Integer id;
    /**
     * 可配送区域code
     */
    private String deliverableAreaCode;

    /**
     * 可配送区域name
     */
    private String deliverableAreaName;

    /**
     * 首个/首重
     */
    private Integer firstItem;

    /**
     * 首个/首重 运费
     */
    private Integer firstAmount;

    /**
     * 续件/续重
     */
    private Integer continuousItem;

    /**
     * 续件/续重 运费
     */
    private Integer continuousAmount;
}
