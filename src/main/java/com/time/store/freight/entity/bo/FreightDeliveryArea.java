package com.time.store.freight.entity.bo;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * <p>
 * 运费模板配送区域
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
@EqualsAndHashCode(callSuper = true)
@TableName("freight_delivery_area")
@Data
public class FreightDeliveryArea extends Model<FreightDeliveryArea> {

    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 运费模板id
     */
    private Integer freightTemplateId;

    /**
     * 可配送区域code
     */
    private String deliverableAreaCode;

    /**
     * 可配送区域name
     */
    private String deliverableAreaName;

    /**
     * 首个/首重
     */
    private Integer firstItem;

    /**
     * 首个/首重 运费
     */
    private Integer firstAmount;

    /**
     * 续件/续重
     */
    private Integer continuousItem;

    /**
     * 续件/续重 运费
     */
    private Integer continuousAmount;

    /**
     * 店铺id
     */
    private Integer storeId;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

}
