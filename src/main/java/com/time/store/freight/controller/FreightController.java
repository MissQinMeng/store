package com.time.store.freight.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.time.store.core.entity.base.BasePageRequest;
import com.time.store.freight.entity.dto.CreateFreightTemplateDTO;
import com.time.store.freight.entity.vo.CreateFreightTemplateVO;
import com.time.store.freight.service.FreightTemplateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 配送管理
 *
 * @description: 配送管理
 * @menu 配送管理
 */
@RestController
@RequestMapping("/api/freight")
public class FreightController {

    @Autowired
    public FreightTemplateService freightTemplateService;

    /**
     * 创建运费模板
     *
     * @param createFreightTemplateDTO
     */
    @PostMapping("/createTemplate")
    public void createTemplate(@RequestBody @Validated CreateFreightTemplateDTO createFreightTemplateDTO) {
        freightTemplateService.createTemplate(createFreightTemplateDTO);
    }

    /**
     * 分页查询运费模板
     *
     * @param basePageRequest
     * @return
     */
    @GetMapping("/page")
    public Page<CreateFreightTemplateVO> page(BasePageRequest basePageRequest) {
        return freightTemplateService.pageTemplate(basePageRequest);
    }

    /**
     * 修改运费模板
     *
     * @param id
     * @param updateFreightTemplateDTO
     */
    @PutMapping("/{id}")
    public void update(@PathVariable("id") Integer id, @RequestBody @Validated CreateFreightTemplateDTO updateFreightTemplateDTO) {
        freightTemplateService.updateTemplate(id, updateFreightTemplateDTO);
    }

    /**
     * 删除运费模板
     *
     * @param id
     */
    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Integer id) {
        freightTemplateService.removeById(id);
    }
}
