package com.time.store.freight.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.time.store.freight.entity.bo.FreightDeliveryArea;
import com.time.store.freight.entity.bo.FreightTemplate;

/**
 * 运费模板配送区域mapper
 */
public interface FreightDeliveryAreaMapper extends BaseMapper<FreightDeliveryArea> {

}
