package com.time.store.freight.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.time.store.core.entity.base.BasePageRequest;
import com.time.store.freight.entity.bo.FreightTemplate;
import com.time.store.freight.entity.vo.CreateFreightTemplateVO;
import com.time.store.order.entity.bo.OrderAfterSale;
import com.time.store.order.entity.dto.AfterSalePageQuery;
import com.time.store.order.entity.dto.OrderAfterSaleExportDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 运费模板mapper
 */
public interface FreightTemplateMapper extends BaseMapper<FreightTemplate> {

    /**
     * 分页查询总条数
     *
     * @return
     */
    int pageCount();

    /**
     * 分页过去模板id
     *
     * @param basePageRequest
     * @return
     */
    List<Integer> pageTemplateIds(BasePageRequest basePageRequest);

    /**
     * 分页查询模板信息
     *
     * @param
     * @return
     */
    List<CreateFreightTemplateVO> pageTemplate(List<Integer> templateIds);

    List<FreightTemplate> selectAllByBillingMethod(@Param("billingMethod")Integer billingMethod);


}
