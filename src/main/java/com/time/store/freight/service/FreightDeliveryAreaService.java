package com.time.store.freight.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.time.store.freight.entity.bo.FreightDeliveryArea;
import com.time.store.freight.entity.bo.FreightTemplate;
import com.time.store.freight.mapper.FreightDeliveryAreaMapper;
import com.time.store.freight.mapper.FreightTemplateMapper;
import org.springframework.stereotype.Service;

@Service
public class FreightDeliveryAreaService extends ServiceImpl<FreightDeliveryAreaMapper, FreightDeliveryArea> implements IService<FreightDeliveryArea> {

}
