package com.time.store.task.entity.dto;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.util.Date;

/**
 * 任务信息
 */
@Data
public class TaskInfoDTO {
    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 任务标题
     */
    private String title;

    /**
     * 任务权重
     */
    private Integer weight;

    /**
     * 任务描述
     */
    private String description;

    /**
     * 任务类别id
     */
    private Integer taskCategoryId;

    /**
     * 时间币
     */
    private Integer timeMoney;

    /**
     * 任务类别标题
     */
    private String taskCategoryName;
    /**
     * 是否领取奖励 0:未领取 1:已领取
     */
    private Integer isReceive;

    /**
     * 是否已经完成 0:未完成 1:已完成
     */
    private Integer isComplete;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;


}
