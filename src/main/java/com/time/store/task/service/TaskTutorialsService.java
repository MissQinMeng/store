package com.time.store.task.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.time.store.task.entity.bo.TaskStore;
import com.time.store.task.entity.bo.TaskTutorials;
import com.time.store.task.mapper.TaskStoreMapper;
import com.time.store.task.mapper.TaskTutorialsMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 任务教程service
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
@Service
public class TaskTutorialsService extends ServiceImpl<TaskTutorialsMapper, TaskTutorials> implements IService<TaskTutorials> {


}
