package com.time.store.news.entity.bo;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * <p>
 * 新闻类别实体
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
@EqualsAndHashCode(callSuper = true)
@TableName("news_category")
@Data
public class NewsCategory extends Model<NewsCategory> {

    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;


    /**
     * 分类名称
     */
    private String name;

    /**
     * 分类权重
     */
    private Integer weight;

    /**
     * 描述
     */
    private String description;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;


}
