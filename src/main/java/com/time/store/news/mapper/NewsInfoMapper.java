package com.time.store.news.mapper;

import com.time.store.news.entity.dto.NewsCategoryDTO;
import com.time.store.news.entity.dto.NewsDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.time.store.news.entity.bo.NewsInfo;

public interface NewsInfoMapper extends BaseMapper<NewsInfo> {

    List<NewsCategoryDTO> selectByPosition(@Param("positionKey") String positionKey);

    List<NewsDTO> selectNewsByPosition(@Param("positionKey") String positionKey);

}
