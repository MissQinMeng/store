package com.time.store.news.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.time.store.news.entity.bo.NewsCategory;

public interface NewsCategoryMapper extends BaseMapper<NewsCategory> {
}
