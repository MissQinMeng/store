package com.time.store.news.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.time.store.news.entity.bo.NewsInfo;
import com.time.store.news.entity.bo.NewsPosition;

public interface NewsPositionMapper extends BaseMapper<NewsPosition> {
}
