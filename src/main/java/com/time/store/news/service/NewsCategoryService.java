package com.time.store.news.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.time.store.news.entity.bo.NewsCategory;
import com.time.store.news.entity.bo.NewsInfo;
import com.time.store.news.mapper.NewsCategoryMapper;
import org.springframework.stereotype.Service;

@Service
public class NewsCategoryService extends ServiceImpl<NewsCategoryMapper, NewsCategory> implements IService<NewsCategory> {
}
