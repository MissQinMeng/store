package com.time.store.core.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "auth")
@Data
public class AuthConfig {
    /**
     * 过期时间
     */
    private Long expire;
    /**
     * 刷新Token过期时间
     */
    private Long refreshExpire;


}