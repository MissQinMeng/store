package com.time.store.core.consts;

public class AuthExceptionConst {
    /**
     * 未找到token
     */
    public static final String NOT_FOUNT = "token未找到";
    /**
     * token失效
     */
    public static final String INVALID = "token失效";
}
