package com.time.store.core.consts;

/**
 * @author 时间
 * @date 2021/3/15 17:21
 */
public class RequestAttributeKeyConst {
    public final static String USERINFO = "userInfo";
    public final static String STOREID = "storeId";

}
