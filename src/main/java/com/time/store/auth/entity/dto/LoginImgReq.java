package com.time.store.auth.entity.dto;

import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Data
public class LoginImgReq {
    /**
     * 图片验证码宽(50-1000)
     */
    @Max(1000)
    @Min(1)
    private Integer width = 200;

    /**
     * 图片验证码高(50-1000)
     */
    @Max(1000)
    @Min(1)
    private Integer height = 100;
}
