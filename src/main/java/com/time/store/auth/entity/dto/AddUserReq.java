package com.time.store.auth.entity.dto;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class AddUserReq {

    /**
     * 登录名
     */
    @NotBlank
    private String loginName;
    /**
     * 昵称
     */
    @NotBlank
    private String nickName;

    /**
     * 密码
     */
    @NotBlank
    private String password;

    /**
     * 密保邮箱
     */
    @NotBlank
    private String protectEmail;

    /**
     * 密保手机号
     */
    @NotBlank
    private String protectPhone;
}
