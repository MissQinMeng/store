package com.time.store.auth.entity.dto;

import com.time.store.core.entity.base.BasePageRequest;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.util.Date;

@Data
public class RolePageQueryReq extends BasePageRequest {
    /**
     * 角色名称
     */
    private String name;
    /**
     * 创建时间开始时间
     */
    private LocalDateTime createTimeStart;
    /**
     * 创建时间结束时间
     */
    private LocalDateTime createTimeEnd;
}
