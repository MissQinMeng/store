package com.time.store.auth.entity.vo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class RoleListRes {
    /**
     * id
     */
    private Integer id;
    /**
     * 是否选中
     */
    private boolean check;

    /**
     * 角色名称
     */
    private String name;
    /**
     * 店铺id
     */
    private Integer storeId;

    private Date createTime;
    private Date updateTime;
}
