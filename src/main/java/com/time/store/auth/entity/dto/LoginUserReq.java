package com.time.store.auth.entity.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class LoginUserReq {
    /**
     * 名称
     */
    @NotBlank
    private String name;
    /**
     * 密码
     */
    @NotBlank
    private String password;

    /**
     * 图形id
     */
    @NotBlank
    private String imgId;
    /**
     * 图形验证码
     */
    @NotBlank
    private String imgCode;

}
