package com.time.store.auth.entity.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class AddUserSetRoleReq {
    /**
     * 用户id
     */
    @NotNull
    private Integer userId;
    /**
     * 角色id
     */
    @NotNull
    private List<Integer> roleIds;

}
