package com.time.store.auth.entity.vo;

import lombok.Data;

@Data
public class ImgCode {
    /**
     * imgBase64位编码
     */
    private String imgBase64;
    /**
     * imgId登录参数需要
     */
    private String imgId;

    /**
     * 图片超时时间(秒)
     */
    private Long expire;
}
