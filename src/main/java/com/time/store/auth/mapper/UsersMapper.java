package com.time.store.auth.mapper;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.time.store.auth.entity.bo.Users;
import com.time.store.auth.entity.dto.UserPageReq;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
public interface UsersMapper extends BaseMapper<Users> {

    @InterceptorIgnore(tenantLine = "true")
    Users selectByLoginName(@Param("loginName") String loginName);

    /**
     * 查询用户信息
     * @param users
     * @return
     */
    Page<Users> pageUser(@Param("page")IPage<UserPageReq> page, @Param("users") UserPageReq users);

}
