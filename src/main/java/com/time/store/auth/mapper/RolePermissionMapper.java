package com.time.store.auth.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.time.store.auth.entity.bo.RolePermission;

/**
 * <p>
 * 角色权限表 Mapper 接口
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
public interface RolePermissionMapper extends BaseMapper<RolePermission> {

}
