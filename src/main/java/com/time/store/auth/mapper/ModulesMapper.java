package com.time.store.auth.mapper;

import cn.hutool.core.lang.tree.Tree;
import com.time.store.auth.entity.vo.ModulesListRes;
import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.time.store.auth.entity.bo.Modules;
import com.time.store.auth.entity.dto.ModulesPageQueryReq;

import java.util.List;

/**
 * <p>
 * 模块表 Mapper 接口
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
public interface ModulesMapper extends BaseMapper<Modules> {
    /**
     * 查询模块
     *
     * @param modulesPageQueryReq
     */
    List<Modules> listModule(@Param("modulesPageQueryReq") ModulesPageQueryReq modulesPageQueryReq);

    /**
     * 根据角色id查询菜单
     * @param roleId
     * @return
     */
    List<ModulesListRes> listByRole(Integer roleId);
}
