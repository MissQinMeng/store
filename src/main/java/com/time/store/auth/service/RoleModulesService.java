package com.time.store.auth.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.time.store.auth.entity.bo.RoleModules;
import com.time.store.auth.mapper.RoleModulesMapper;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色模块表 服务实现类
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
@Service
public class RoleModulesService extends ServiceImpl<RoleModulesMapper, RoleModules> implements IService<RoleModules> {

}
