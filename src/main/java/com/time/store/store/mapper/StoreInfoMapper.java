package com.time.store.store.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.time.store.auth.entity.bo.Modules;
import com.time.store.store.entity.bo.StoreInfo;

/**
 * <p>
 * 店铺信息 Mapper 接口
 * </p>
 *
 * @author 时间
 * @since 2021-06-27
 */
public interface StoreInfoMapper extends BaseMapper<StoreInfo> {

}
