package com.time.store.product.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.time.store.core.annotations.IgnoreAuth;
import com.time.store.product.entity.dto.ProductEvaluateChoiceDTO;
import com.time.store.product.entity.dto.ProductEvaluateReplyDTO;
import com.time.store.product.entity.dto.ProductEvaluateTopDTO;
import com.time.store.product.entity.query.ProductEvaluateQuery;
import com.time.store.product.entity.vo.ProductEvaluatePageVO;
import com.time.store.product.service.ProductEvaluateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 商品评价控制器
 *
 * @description: 商品评价控制器
 * @menu 商品评价
 */
@RestController
@RequestMapping("/api/product/evaluate")
public class ProductEvaluateController {

    @Autowired
    private ProductEvaluateService productEvaluateService;

    /**
     * 分页查询
     *
     * @param productEvaluateQuery
     * @return
     * @status done
     */
    @GetMapping("/page")
    public Page<ProductEvaluatePageVO> page(@Validated ProductEvaluateQuery productEvaluateQuery) {
        return productEvaluateService.pageEvaluate(productEvaluateQuery);
    }

    /**
     * 精选
     *
     * @param productEvaluateChoiceDTO
     * @return
     * @status done
     */
    @PatchMapping("/choice")
    public void choice(@RequestBody @Validated ProductEvaluateChoiceDTO productEvaluateChoiceDTO) {
         productEvaluateService.choice(productEvaluateChoiceDTO);
    }

    /**
     * 置顶
     *
     * @param productEvaluateTopDTO
     * @return
     * @status done
     */
    @PatchMapping("/top")
    public void top(@RequestBody @Validated ProductEvaluateTopDTO productEvaluateTopDTO) {
        productEvaluateService.top(productEvaluateTopDTO);
    }

    /**
     * 回复
     *
     * @param productEvaluateReplyDTO
     * @return
     * @status done
     */
    @PostMapping("/reply")
    public void reply(@RequestBody @Validated ProductEvaluateReplyDTO productEvaluateReplyDTO) {
        productEvaluateService.reply(productEvaluateReplyDTO);
    }

}
