package com.time.store.product.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.time.store.core.util.AuthUtil;
import com.time.store.core.util.NoCardUtil;
import com.time.store.order.entity.bo.OrderShipment;
import com.time.store.product.entity.bo.ProductInfo;
import com.time.store.product.entity.bo.ProductStoreReply;
import com.time.store.product.entity.dto.ProductInfoAddDTO;
import com.time.store.product.entity.dto.ProductInfoPutOnTheShelfDTO;
import com.time.store.product.entity.dto.ProductInfoUpdateDTO;
import com.time.store.product.entity.query.ProductQuery;
import com.time.store.product.mapper.ProductInfoMapper;
import com.time.store.product.mapper.ProductStoreReplyMapper;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

@Service
public class ProductInfoService extends ServiceImpl<ProductInfoMapper, ProductInfo> implements IService<ProductInfo> {
    /**
     * 新增商品
     *
     * @param productInfoAddDTO
     */
    public void saveProduct(ProductInfoAddDTO productInfoAddDTO) {
        ProductInfo productInfo = new ProductInfo();
        BeanUtils.copyProperties(productInfoAddDTO, productInfo);
        productInfo.setProductNo(generateProductCode());
        productInfo.setStoreId(AuthUtil.getCurrStoreId());
        save(productInfo);
    }




    public void updateProduct(ProductInfoUpdateDTO productInfoUpdateDTO) {
        ProductInfo productInfo = new ProductInfo();
        BeanUtils.copyProperties(productInfoUpdateDTO, productInfo);
        updateById(productInfo);
    }




    private String generateProductCode() {

        String productNo = null;
        do {
            productNo = NoCardUtil.generateNo("PN");
            // 查询数据库是否已有单号
            Integer shippingCodeNoCount = lambdaQuery().eq(ProductInfo::getProductNo, productNo).count();
            if (shippingCodeNoCount > 0) {
                productNo = null;
            }
        } while (productNo == null);


        return productNo;
    }

    /**
     * 上/下架商品
     * @param productInfoPutOnTheShelfDTO
     */
    public void putOnTheShelf(ProductInfoPutOnTheShelfDTO productInfoPutOnTheShelfDTO) {
        ProductInfo productInfo = new ProductInfo();
        BeanUtils.copyProperties(productInfoPutOnTheShelfDTO, productInfo);
        updateById(productInfo);
    }

    /**
     * 分页查询商品
     * @param productQuery
     * @return
     */
    public Page<ProductInfo> pageProduct(ProductQuery productQuery) {
        Page<ProductInfo> page=new Page<>(productQuery.getCurrent(),productQuery.getSize());

        return baseMapper.pageProduct(productQuery,page);
    }
}
