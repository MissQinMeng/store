package com.time.store.product.entity.bo;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * <p>
 * 商品评价表
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
@EqualsAndHashCode(callSuper = true)
@TableName("product_evaluate")
@Data
public class ProductEvaluate extends Model<ProductEvaluate> {
    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 商品id
     */
    private Integer productId;

    /**
     * 商品名称
     */
    private Integer productName;

    /**
     * 评价用户id
     */
    private Integer membersId;

    /**
     * 订单id
     */
    private Integer orderId;

    /**
     * 订单code
     */
    private String orderNo;

    /**
     * 订单详情id
     */
    private Integer orderDetailId;

    /**
     * 评价内容
     */
    private String content;

    /**
     * 评价方式 1:默认评价 2:用户自评
     */
    private int evaluateManner;

    /**
     * 评价星级 0-5
     */
    private int star;

    /**
     * 是否置顶 1:置顶 2:不置顶
     */
    private int isTop;

    /**
     * 是否加精 1:加精 2:不加精
     */
    private int isChoice;

    /**
     * 图片数量
     */
    private Integer imgNum;

    /**
     * 店铺id
     */
    private Integer storeId;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;
}
