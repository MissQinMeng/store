package com.time.store.product.entity.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * 回复
 */
@Data
public class ProductEvaluateReplyDTO {
    /**
     * 回复id
     */
    @NotNull
    private Integer id;

    /**
     * 回复内容
     */
    @NotEmpty
    private String content;
}
