package com.time.store.product.entity.vo;


import cn.hutool.core.util.StrUtil;
import lombok.Data;

import java.util.Date;

@Data
public class ProductEvaluatePageVO {
    /**
     * id
     */
    private Integer id;

    /**
     * 商品id
     */
    private Integer productId;

    /**
     * 商品名称
     */
    private Integer productName;

    /**
     * 评价用户id
     */
    private Integer membersId;

    /**
     * 订单id
     */
    private Integer orderId;

    /**
     * 订单code
     */
    private String orderNo;

    /**
     * 订单详情id
     */
    private Integer orderDetailId;

    /**
     * 评价内容
     */
    private String content;

    /**
     * 评价方式 1:默认评价 2:用户自评
     */
    private int evaluateManner;

    /**
     * 评价星级 0-5
     */
    private int star;

    /**
     * 是否置顶 1:置顶 2:不置顶
     */
    private int isTop;

    /**
     * 是否加精 1:加精 2:不加精
     */
    private int isChoice;

    /**
     * 图片数量
     */
    private Integer imgNum;

    /**
     * 店铺id
     */
    private Integer storeId;

    private Date createTime;
    private Date updateTime;

    /**
     * 商品sku
     */
    private String productSku;
    /**
     * 评价人昵称
     */
    private String membersNickName;

    /**
     * 商家回复内容
     */
    private String storeContent;

    /**
     * 商家回复时间
     */
    private Date storeContentCreate;


    public String getMembersNickName() {
        String encryptNickName = "";
        if (StrUtil.isEmpty(membersNickName))
            return encryptNickName;

        if (membersNickName.length() > 2) {
            encryptNickName = membersNickName.charAt(0) + "**" + membersNickName.charAt(membersNickName.length() - 1);
        } else if (membersNickName.length() == 2) {
            encryptNickName = membersNickName.charAt(0) + "**";
        } else {
            encryptNickName = "**";
        }
        return encryptNickName;
    }
}
