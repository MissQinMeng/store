package com.time.store.product.entity.query;

import com.time.store.core.entity.base.BasePageRequest;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;

/**
 * 商品分页查询
 */
@Data
public class ProductQuery extends BasePageRequest {
    /**
     * 创建时间开始
     */
    private LocalDateTime createTimeStart;
    /**
     * 创建时间结束
     */
    private LocalDateTime createTimeEnd;
    /**
     * 商品名称
     */
    private String productName;
    /**
     * 商品分类id
     */
    private Integer productCategoryId;
    /**
     * 价格区间开始
     */
    private BigDecimal priceStart;
    /**
     * 价格区间结束
     */
    private BigDecimal priceEnd;

    public BigDecimal getPriceStart() {
        if(priceStart == null)
            return null;
        return priceStart.multiply(BigDecimal.valueOf(100));
    }

    public BigDecimal getPriceEnd() {
        if(priceEnd == null)
            return null;
        return priceEnd.multiply(BigDecimal.valueOf(100));
    }
}
