package com.time.store.setting.serivice;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.time.store.setting.entity.po.SettingPay;
import com.time.store.setting.entity.po.SettingProduct;
import com.time.store.setting.mapper.SettingPayMapper;
import com.time.store.setting.mapper.SettingProductMapper;
import org.springframework.stereotype.Service;

@Service
public class SettingProductService extends ServiceImpl<SettingProductMapper, SettingProduct> implements IService<SettingProduct> {
}
