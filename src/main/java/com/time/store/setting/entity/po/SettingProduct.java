package com.time.store.setting.entity.po;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * <p>
 * 商品设置表
 * </p>
 *
 * @author 时间
 * @since 2021-04-25
 */
@EqualsAndHashCode(callSuper = true)
@TableName("setting_product")
@Data
public class SettingProduct extends Model<SettingProduct> {

    /**
     * id
     */
    @TableId(type = IdType.AUTO)
    private Integer id;

    /**
     * 为你推荐 (1: 开启 2:关闭)
     */
    private int recommendedToYou;

    /**
     * 商品评价 (1: 展示全网评价 2:隐藏全网评价)
     */
    private int productReview;

    /**
     * 好评率展示
     */
    private int favorableRateDisplay;

    /**
     * 评价会员标签展示 (1:评价展示会员等级 2:评价展示会员名称)
     */
    private int evaluationMemberLabelDisplay;

    /**
     * 评价排序 (1:智能推荐排序 2:评价产生时间排序)
     */
    private int evaluationMemberOrder;

    /**
     * 更多商品推荐 (1:开启 2:关闭)
     */
    private int moreProductRecommendation;

    /**
     * 更多商品推荐-商品详情页 (1:开启 2:关闭)
     */
    private int productRecommendationProductDetail;

    /**
     * 更多商品推荐-购物车 (1:开启 2:关闭)
     */
    private int productRecommendationShoppingCart;

    /**
     * 更多商品推荐-支付成功页 (1:开启 2:关闭)
     */
    private int productRecommendationShoppingPaymentSuccessful;

    /**
     * 更多商品推荐-订单列表 (1:开启 2:关闭)
     */
    private int productRecommendationShoppingOrderList;

    /**
     * 更多商品推荐-订单详情 (1:开启 2:关闭)
     */
    private int productRecommendationShoppingOrderDetail;

    /**
     * 更多商品推荐-权益卡页 (1:开启 2:关闭)
     */
    private int productRecommendationShoppingEntitlementcard;

    /**
     * 更多商品推荐-物流详情页 (1:开启 2:关闭)
     */
    private int productRecommendationShoppingLogisticsDetail;

    /**
     * 更多商品推荐-退款详情页 (1:开启 2:关闭)
     */
    private int productRecommendationShoppingRefundDetail;

    /**
     * 更多商品推荐-免费会员中心页 (1:开启 2:关闭)
     */
    private int productRecommendationShoppingFreeMemberCenter;

    /**
     * 更多商品推荐-付费会员中心页 (1:开启 2:关闭)
     */
    private int productRecommendationShoppingPaidMemberCenter;

    /**
     * 商详页店铺按钮 (1:展示 2:不展示)
     */
    private int productDetailPageShopButton;

    /**
     * 商详页立即购买 (1:展示 2:不展示)
     */
    private int productDetailPageBuyNow;

    /**
     * 商详页立即购买按钮名称
     */
    private String productDetailPageBuyNowName;

    /**
     * 商详页销量 (1:展示 2:不展示)
     */
    private int productDetailSales;

    /**
     * 商详页销量达到多少显示
     */
    private Integer productDetailSalesAchieve;

    /**
     * 主图视频好评弹幕 (1:展示 2:不展示)
     */
    private int mainPictureVideoPraiseBarrage;

    /**
     * 商详页展示列表展示成交记录 (1:展示 2:不展示)
     */
    private int productDetailListDisplayTransactions;

    /**
     * 商品海报样式 (1:样式一 2:样式二 3:样式三)
     */
    private int commodityPosterStyle;

    /**
     * 商品海报分享人 (1:展示 2:不展示)
     */
    private int commodityPosterSharer;

    /**
     * 小程序分享卡牌样式海 (1:基础样式1 2:基础样式2 3:促销样式)
     */
    private int miniProgramShareCardStyle;

    /**
     * 小程序分享标题 (1:仅展示商品名称 2:展示智能文案 3:展示自定义前缀)
     */
    private int miniProgramShareTitle;

    /**
     * 小程序分享标题自定义前缀
     */
    private String miniProgramShareTitlePrefix;

    /**
     * 商品补货提醒 (1:展示 2:不展示)
     */
    private int productRemindReplenishment;

    /**
     * 商品开售提醒 (1:展示 2:不展示)
     */
    private int productRemindOnSale;

    /**
     * 商品详情页展示活动倒计时 (1:展示 2:不展示)
     */
    private int productDetailDisplay;

    /**
     * 商品详情页具体展示活动倒计时
     */
    private Integer productDetailDisplayEventCountdown;

    /**
     * 商品详情页面展示类目参数 (1:展示 2:不展示)
     */
    private int productDetailsDisplayCategoryParameters;

    /**
     * 领券促销入口 (1:前置展示 2:不前置展示)
     */
    private int couponPromotionEntrance;

    /**
     * 店铺id
     */
    private Integer storeId;

    @TableField(fill = FieldFill.INSERT)
    private Date createTime;
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;


}
