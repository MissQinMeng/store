package com.time.store.setting.mapper;

import com.time.store.setting.entity.po.SettingOrder;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 订单设置mapper
 */
public interface SettingOrderMapper extends BaseMapper<SettingOrder> {

}
