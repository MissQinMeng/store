package com.time.store.setting.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.time.store.setting.entity.po.SettingPay;
import com.time.store.setting.entity.po.SettingProduct;

/**
 * 商品设置mapper
 */
public interface SettingProductMapper extends BaseMapper<SettingProduct> {

}
