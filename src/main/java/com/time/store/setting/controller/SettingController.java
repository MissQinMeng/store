package com.time.store.setting.controller;

import com.time.store.setting.entity.po.SettingOrder;
import com.time.store.setting.entity.po.SettingPay;
import com.time.store.setting.entity.po.SettingProduct;
import com.time.store.setting.entity.po.SettingStore;
import com.time.store.setting.serivice.SettingOrderService;
import com.time.store.setting.serivice.SettingPayService;
import com.time.store.setting.serivice.SettingProductService;
import com.time.store.setting.serivice.SettingStoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

/**
 * 设置控制器
 *
 * @description: 设置
 * @menu 设置
 */
@RestController
@RequestMapping("/api/setting")
public class SettingController {
    @Autowired
    private SettingOrderService settingOrderService;
    @Autowired
    private SettingPayService settingPayService;
    @Autowired
    private SettingProductService settingProductService;
    @Autowired
    private SettingStoreService settingStoreService;


    /**
     * 获取订单设置信息
     *
     * @return
     * @status done
     */
    @GetMapping("/order")
    public SettingOrder orderList() {
        return settingOrderService.getOne(null,true);
    }

    /**
     * 获取支付设置信息
     *
     * @return
     * @status done
     */
    @GetMapping("/pay")
    public SettingPay payList() {
        return settingPayService.getOne(null,true);
    }

    /**
     * 获取商品设置信息
     *
     * @return
     * @status done
     */
    @GetMapping("/product")
    public SettingProduct productList() {
        return settingProductService.getOne(null,true);
    }

    /**
     * 获取店铺设置信息
     *
     * @return
     * @status done
     */
    @GetMapping("/store")
    public SettingStore storeList() {
        return settingStoreService.getOne(null,true);
    }


    /**
     * 修改订单设置信息
     *
     * @return
     * @status done
     */
    @PutMapping("/order/{id}")
    public void updateOrder(@RequestBody @Validated SettingOrder settingOrder, @PathVariable("id") Integer id) {
        settingOrder.setId(id);
        settingOrder.setStoreId(null);
        settingOrder.setCreateTime(null);
        settingOrder.setUpdateTime(null);
        settingOrderService.updateById(settingOrder);
    }

    /**
     * 修改支付设置信息
     *
     * @return
     * @status done
     */
    @PutMapping("/pay/{id}")
    public void updatePay(@RequestBody @Validated SettingPay settingPay, @PathVariable("id") Integer id) {
        settingPay.setId(id);
        settingPay.setStoreId(null);
        settingPay.setCreateTime(null);
        settingPay.setUpdateTime(null);
        settingPayService.updateById(settingPay);
    }

    /**
     * 修改商品设置信息
     *
     * @return
     * @status done
     */
    @PutMapping("/product/{id}")
    public void updateProduct(@RequestBody @Validated SettingProduct settingProduct, @PathVariable("id") Integer id) {
        settingProduct.setId(id);
        settingProduct.setStoreId(null);
        settingProduct.setCreateTime(null);
        settingProduct.setUpdateTime(null);
        settingProductService.updateById(settingProduct);
    }

    /**
     * 修改店铺设置信息
     *
     * @return
     * @status done
     */
    @PutMapping("/store/{id}")
    public void updateStore(@RequestBody @Validated SettingStore settingStore, @PathVariable("id") Integer id) {
        settingStore.setId(id);
        settingStore.setStoreId(null);
        settingStore.setCreateTime(null);
        settingStore.setUpdateTime(null);
        settingStoreService.updateById(settingStore);
    }

}
