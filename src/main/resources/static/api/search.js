let api = [];
api.push({
    alias: 'OrderShipmentController',
    order: '1',
    link: '订单发货信息控制器',
    desc: '订单发货信息控制器',
    list: []
})
api[0].list.push({
    order: '1',
    desc: '分页查询发货信息',
});
api[0].list.push({
    order: '2',
    desc: '单个发货',
});
api[0].list.push({
    order: '3',
    desc: '批量发货模板下载',
});
api[0].list.push({
    order: '4',
    desc: '发货订单批量导入',
});
api.push({
    alias: 'OrderInfoController',
    order: '2',
    link: '订单信息控制器',
    desc: '订单信息控制器',
    list: []
})
api[1].list.push({
    order: '1',
    desc: '分页查询订单',
});
api[1].list.push({
    order: '2',
    desc: '导出订单',
});
api[1].list.push({
    order: '3',
    desc: '加星订单',
});
api.push({
    alias: 'OrderStaticalController',
    order: '3',
    link: '订单统计控制器',
    desc: '订单统计控制器',
    list: []
})
api[2].list.push({
    order: '1',
    desc: '获取店铺订单统计信息',
});
api[2].list.push({
    order: '2',
    desc: '订单概况统计',
});
api.push({
    alias: 'OrderAfterSaleController',
    order: '4',
    link: '订单售后控制器',
    desc: '订单售后控制器',
    list: []
})
api[3].list.push({
    order: '1',
    desc: '分页查询',
});
api[3].list.push({
    order: '2',
    desc: '导出售后',
});
api[3].list.push({
    order: '3',
    desc: '同意/拒绝退款',
});
api.push({
    alias: 'ExpandController',
    order: '5',
    link: '扩展控制器',
    desc: '扩展控制器',
    list: []
})
api[4].list.push({
    order: '1',
    desc: '修改菜单信息',
});
api.push({
    alias: 'RoleController',
    order: '6',
    link: '角色控制器',
    desc: '角色控制器',
    list: []
})
api[5].list.push({
    order: '1',
    desc: '保存角色',
});
api[5].list.push({
    order: '2',
    desc: '修改角色',
});
api[5].list.push({
    order: '3',
    desc: '删除角色',
});
api[5].list.push({
    order: '4',
    desc: '分页查询角色',
});
api[5].list.push({
    order: '5',
    desc: '根据用户id查询所有角色',
});
api[5].list.push({
    order: '6',
    desc: '根据用户id设置所有角色',
});
api.push({
    alias: 'UsersController',
    order: '7',
    link: '用户控制器',
    desc: '用户控制器',
    list: []
})
api[6].list.push({
    order: '1',
    desc: '用户查询',
});
api[6].list.push({
    order: '2',
    desc: '用户增加',
});
api[6].list.push({
    order: '3',
    desc: '用户修改',
});
api[6].list.push({
    order: '4',
    desc: '用户删除',
});
api[6].list.push({
    order: '5',
    desc: '修改密码',
});
api[6].list.push({
    order: '6',
    desc: '用户个人设置',
});
api.push({
    alias: 'ModulesController',
    order: '8',
    link: '菜单控制器',
    desc: '菜单控制器',
    list: []
})
api[7].list.push({
    order: '1',
    desc: '保存菜单',
});
api[7].list.push({
    order: '2',
    desc: '修改菜单',
});
api[7].list.push({
    order: '3',
    desc: '删除菜单',
});
api[7].list.push({
    order: '4',
    desc: '查询树形菜单',
});
api[7].list.push({
    order: '5',
    desc: '根据角色id查询树形菜单',
});
api[7].list.push({
    order: '6',
    desc: '修改菜单根据角色',
});
api.push({
    alias: 'LoginController',
    order: '9',
    link: '登录控制器',
    desc: '登录控制器',
    list: []
})
api[8].list.push({
    order: '1',
    desc: '获取登录图形验证码',
});
api[8].list.push({
    order: '2',
    desc: '登录',
});
api[8].list.push({
    order: '3',
    desc: '获取当前用户信息',
});
api[8].list.push({
    order: '4',
    desc: '刷新token',
});
api.push({
    alias: 'FreightController',
    order: '10',
    link: '配送管理',
    desc: '配送管理',
    list: []
})
api[9].list.push({
    order: '1',
    desc: '创建运费模板',
});
api[9].list.push({
    order: '2',
    desc: '分页查询运费模板',
});
api[9].list.push({
    order: '3',
    desc: '修改运费模板',
});
api[9].list.push({
    order: '4',
    desc: '删除运费模板',
});
api.push({
    alias: 'ProductController',
    order: '11',
    link: '商品控制器',
    desc: '商品控制器',
    list: []
})
api[10].list.push({
    order: '1',
    desc: '分页查询',
});
api[10].list.push({
    order: '2',
    desc: '新增商品',
});
api[10].list.push({
    order: '3',
    desc: '修改商品',
});
api[10].list.push({
    order: '4',
    desc: '删除商品',
});
api[10].list.push({
    order: '5',
    desc: '上/下架商品',
});
api.push({
    alias: 'ProductEvaluateController',
    order: '12',
    link: '商品评价控制器',
    desc: '商品评价控制器',
    list: []
})
api[11].list.push({
    order: '1',
    desc: '分页查询',
});
api[11].list.push({
    order: '2',
    desc: '精选',
});
api[11].list.push({
    order: '3',
    desc: '置顶',
});
api[11].list.push({
    order: '4',
    desc: '回复',
});
api.push({
    alias: 'NewsController',
    order: '13',
    link: '新闻控制器',
    desc: '新闻控制器',
    list: []
})
api[12].list.push({
    order: '1',
    desc: '获取首页右边新闻',
});
api[12].list.push({
    order: '2',
    desc: '获取功能位置新闻',
});
api.push({
    alias: 'TaskController',
    order: '14',
    link: '任务',
    desc: '任务',
    list: []
})
api[13].list.push({
    order: '1',
    desc: '获取当前店铺任务',
});
api.push({
    alias: 'SettingController',
    order: '15',
    link: '设置控制器',
    desc: '设置控制器',
    list: []
})
api[14].list.push({
    order: '1',
    desc: '获取订单设置信息',
});
api[14].list.push({
    order: '2',
    desc: '获取支付设置信息',
});
api[14].list.push({
    order: '3',
    desc: '获取商品设置信息',
});
api[14].list.push({
    order: '4',
    desc: '获取店铺设置信息',
});
api[14].list.push({
    order: '5',
    desc: '修改订单设置信息',
});
api[14].list.push({
    order: '6',
    desc: '修改支付设置信息',
});
api[14].list.push({
    order: '7',
    desc: '修改商品设置信息',
});
api[14].list.push({
    order: '8',
    desc: '修改店铺设置信息',
});
api.push({
    alias: 'dict',
    order: '16',
    link: 'dict_list',
    desc: '数据字典',
    list: []
})
document.onkeydown = keyDownSearch;
function keyDownSearch(e) {
    const theEvent = e;
    const code = theEvent.keyCode || theEvent.which || theEvent.charCode;
    if (code == 13) {
        const search = document.getElementById('search');
        const searchValue = search.value;
        let searchArr = [];
        for (let i = 0; i < api.length; i++) {
            let apiData = api[i];
            const desc = apiData.desc;
            if (desc.indexOf(searchValue) > -1) {
                searchArr.push({
                    order: apiData.order,
                    desc: apiData.desc,
                    link: apiData.link,
                    list: apiData.list
                });
            } else {
                let methodList = apiData.list || [];
                let methodListTemp = [];
                for (let j = 0; j < methodList.length; j++) {
                    const methodData = methodList[j];
                    const methodDesc = methodData.desc;
                    if (methodDesc.indexOf(searchValue) > -1) {
                        methodListTemp.push(methodData);
                        break;
                    }
                }
                if (methodListTemp.length > 0) {
                    const data = {
                        order: apiData.order,
                        desc: apiData.desc,
                        link: apiData.link,
                        list: methodListTemp
                    };
                    searchArr.push(data);
                }
            }
        }
        let html;
        if (searchValue == '') {
            const liClass = "";
            const display = "display: none";
            html = buildAccordion(api,liClass,display);
            document.getElementById('accordion').innerHTML = html;
        } else {
            const liClass = "open";
            const display = "display: block";
            html = buildAccordion(searchArr,liClass,display);
            document.getElementById('accordion').innerHTML = html;
        }
        const Accordion = function (el, multiple) {
            this.el = el || {};
            this.multiple = multiple || false;
            const links = this.el.find('.dd');
            links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown);
        };
        Accordion.prototype.dropdown = function (e) {
            const $el = e.data.el;
            $this = $(this), $next = $this.next();
            $next.slideToggle();
            $this.parent().toggleClass('open');
            if (!e.data.multiple) {
                $el.find('.submenu').not($next).slideUp("20").parent().removeClass('open');
            }
        };
        new Accordion($('#accordion'), false);
    }
}

function buildAccordion(apiData, liClass, display) {
    let html = "";
    let doc;
    if (apiData.length > 0) {
        for (let j = 0; j < apiData.length; j++) {
            html += '<li class="'+liClass+'">';
            html += '<a class="dd" href="#_' + apiData[j].link + '">' + apiData[j].order + '.&nbsp;' + apiData[j].desc + '</a>';
            html += '<ul class="sectlevel2" style="'+display+'">';
            doc = apiData[j].list;
            for (let m = 0; m < doc.length; m++) {
                html += '<li><a href="#_' + apiData[j].order + '_' + doc[m].order + '_' + doc[m].desc + '">' + apiData[j].order + '.' + doc[m].order + '.&nbsp;' + doc[m].desc + '</a> </li>';
            }
            html += '</ul>';
            html += '</li>';
        }
    }
    return html;
}