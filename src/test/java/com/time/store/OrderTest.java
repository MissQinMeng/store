package com.time.store;

import cn.hutool.core.util.RandomUtil;
import com.alibaba.fastjson.JSONObject;
import com.time.store.core.util.AuthUtil;
import com.time.store.order.entity.bo.OrderDetail;
import com.time.store.order.entity.bo.OrderInfo;
import com.time.store.order.service.OrderDetailService;
import com.time.store.order.service.OrderInfoService;
import com.time.store.product.entity.bo.ProductInfo;
import com.time.store.product.service.ProductInfoService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class OrderTest {

    @Autowired
    private OrderInfoService orderInfoService;
    @Autowired
    private OrderDetailService orderDetailService;
    @Autowired
    private ProductInfoService productInfoService;

    @Test
    public void orderBatchSave() {
        for (int i = 0; i < 10000; i++) {
            OrderInfo orderInfo = new OrderInfo();
            orderInfo.setOrderNo(orderInfoService.generateOrderNo());
            orderInfo.setExternalPayNo(orderInfoService.generateExternalPayNo());
            orderInfo.setStatus(RandomUtil.randomInt(1, 8));
            orderInfo.setRightsProtectionStatus(RandomUtil.randomInt(1, 3));
            orderInfo.setReceiverName(RandomUtil.randomEle(List.of("火星哥", "时间", "豪大", "小羊", "五九", "小红")));
            orderInfo.setReceiverPhone("15238311715");
            orderInfo.setReceiverPhoneLastFour("1715");
            orderInfo.setPayMethod(RandomUtil.randomInt(1, 4));
            orderInfo.setPayStarred(RandomUtil.randomEle(List.of(0, 1, 2, 3, 4, 5, -1)));
            orderInfo.setType(RandomUtil.randomInt(1, 11));
            orderInfo.setSource(RandomUtil.randomInt(1, 6));
            orderInfo.setDeliveryMethod(RandomUtil.randomInt(1, 3));
            orderInfo.setMemberId(1);
            orderInfo.setStoreId(1);
            orderInfoService.save(orderInfo);

            AuthUtil.setRequestStoreId(1);

            List<OrderDetail> orderDetailList=new ArrayList<>();
            int detailCount = RandomUtil.randomInt(1, 4);
            for (int j = 0; j < detailCount; j++) {
                OrderDetail orderDetail = new OrderDetail();
                orderDetail.setOrderId(orderInfo.getId());
                orderDetail.setOrderNo(orderInfo.getOrderNo());

                ProductInfo productInfo = productInfoService.getById(RandomUtil.randomInt(1, 4));
                orderDetail.setProductId(productInfo.getId());
                orderDetail.setProductName(productInfo.getProductName());
                orderDetail.setProductNo(productInfo.getProductNo());
                orderDetail.setQty(RandomUtil.randomInt(1, 100));
                orderDetail.setReceivableAmount(RandomUtil.randomInt(100, 10000));
                orderDetail.setPaidAmount(RandomUtil.randomInt(1, orderDetail.getReceivableAmount()));
                orderDetail.setFavorableAmount(orderDetail.getReceivableAmount() - orderDetail.getPaidAmount());
                orderDetail.setProductSku(new JSONObject().toJSONString());
                orderDetail.setStoreId(1);

                orderDetailService.save(orderDetail);
                orderDetailList.add(orderDetail);
            }


            orderInfo.setTotalQty((int) orderDetailList.stream().mapToInt(item -> item.getQty()).summaryStatistics().getSum());
            orderInfo.setTotalReceivableAmount((int) orderDetailList.stream().mapToInt(item -> item.getReceivableAmount()).summaryStatistics().getSum());
            orderInfo.setTotalFavorableAmount((int) orderDetailList.stream().mapToInt(item -> item.getFavorableAmount()).summaryStatistics().getSum());
            orderInfo.setTotalPaidAmount((int) orderDetailList.stream().mapToInt(item -> item.getPaidAmount()).summaryStatistics().getSum());
            orderInfoService.updateById(orderInfo);
        }
    }
}
