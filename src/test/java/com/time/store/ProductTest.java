package com.time.store;

import cn.hutool.core.io.FileUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.time.store.product.entity.bo.ProductCategory;
import com.time.store.product.service.ProductCategoryService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class ProductTest {

    @Autowired
    private ProductCategoryService productCategoryService;

    @Test
    public void categoryBatchSave() {
        String s = FileUtil.readString("/Users/shijian/IdeaProjects/store-server/category.txt", "utf-8");
        JSONArray json = JSON.parseArray(s);
        List<ProductCategory> list = new ArrayList<>();
        for (Object o : json) {
            JSONObject jo = (JSONObject) o;

            ProductCategory productCategory = new ProductCategory();
            productCategory.setId(jo.getInteger("id"));
            productCategory.setCategoryName(jo.getString("name"));
            productCategory.setCategoryCode(jo.getString("ruleId"));
            productCategory.setCategoryDesc(jo.getString("name"));
            productCategory.setCategoryOrder(jo.getInteger("priority"));
            productCategory.setParentId(jo.getInteger("parentId"));
            productCategory.setCategoryLevel(jo.getInteger("level"));
            productCategory.setCategoryStatus(jo.getInteger("status"));
            productCategory.setIconUrl("");
            productCategory.setPicUrl("");

            productCategoryService.save(productCategory);
            list.add(productCategory);
        }
    }

}